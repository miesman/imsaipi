# README #

CP/M 8080 C code to calculate the digits of PI

### How do I get set up? ###

Clone this repo and copy the disk image or the imsaipi.com binary to your IMSAI.

Run the imsaipi.com binary. By default it will calculate pi to 7000 digits. You
can calculate it to shorter lengths by adding the length after the command (e.g
imsaipi 100).

Calculating shorter lengths will speed up the processing time.

This code was built with the High Tech C compiler from code I found on the Internet.
See the source file imsai.c for more information.
