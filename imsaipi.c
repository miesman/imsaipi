/*
 * CP/M C source code to calulate digits of PI
 *
 * Orignal code from http://www.codecodex.com/wiki/Calculate_digits_of_pi#C
 *
 * Tested with: "HI-TECH C COMPILER (CP/M-80) V3.09"
 *
 * To compile:
 *
 * "c imsaipi.c"
 *
 * (-O flag breaks the code so don't use it).
 */

#include <stdlib.h>

#define MAXDIGITS 7000

#define SCALE 1000
#define ARRINIT 200

#define LEDSCALE 100
#define UPDATE_SCALE 100

void pi_digits(int digits) {
    int leds = 0;
    long carry = 0;
    int arr[(MAXDIGITS*3) + 1];
    long i;
    long digit_count = 0;
    unsigned char k;
    long last_update = 0;

    for ( i = 0; i <= digits; ++i)
        arr[i] = ARRINIT;
        
    for (i = digits; i > 0; i-= 14) {
        long sum = 0;
	long j;

        for (j = i; j > 0; --j) {
            sum = sum * j + SCALE * (long)arr[j];
            arr[j] = sum % (j * 2 - 1);
            sum /= j * 2 - 1;
	    if (!(j % 50)) {
		leds = leds?0:1;
		
		k = digit_count / LEDSCALE & 0x3f;
		k |= leds ? 0x80:0x40;

		out (0xff, ~k);
	    }
        }

        printf("%03d", carry + sum / SCALE);
        carry = sum % SCALE;
	digit_count += 3;
	if (last_update + UPDATE_SCALE <= digit_count) {
		last_update += UPDATE_SCALE;
		printf("\n %d digits calculated so far\n\n", digit_count);
	}
    }
}

int main(int argc, char** argv) {
    int n = argc == 2 ? atoi(argv[1]) : MAXDIGITS;

    if (n > MAXDIGITS) {
	printf("usage: pi <digits>\n");
	printf("\n <digits> must be less than %d\n", MAXDIGITS);
	exit(1);
    }

    n *=3;
    pi_digits(n);

    return 0;
}
